//
//  HtO.swift
//  GOYFA
//
//  Created by Kevin Crowl on 5/3/15.
//  Copyright (c) 2015 Kevin Crowl. All rights reserved.
//

import UIKit
import CoreData

class HtO: UIViewController {
    
    // Core Data Object
    let managedObjectContext = (UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext
   
    
    @IBOutlet weak var iconOne: UIButton!
    @IBAction func icon1(sender: AnyObject) {
        if lastUsed == 1 {
            lastUsed = lastUsed - 1
            value.removeAtIndex(lastUsed)
            iconOne.setImage(nil, forState: .Normal)
            updateToGo()
        }
    }
    
    @IBOutlet weak var iconTwo: UIButton!
    @IBAction func icon2(sender: AnyObject) {
        if lastUsed == 2 {
            lastUsed = lastUsed - 1
            value.removeAtIndex(lastUsed)
            iconTwo.setImage(nil, forState: .Normal)
            updateToGo()
        }
    }
    
    @IBOutlet weak var iconThree: UIButton!
    @IBAction func icon3(sender: AnyObject) {
        if lastUsed == 3 {
            lastUsed = lastUsed - 1
            value.removeAtIndex(lastUsed)
            iconThree.setImage(nil, forState: .Normal)
            updateToGo()
        }
    }
    
    @IBOutlet weak var iconFour: UIButton!
    @IBAction func icon4(sender: AnyObject) {
        if lastUsed == 4 {
            lastUsed = lastUsed - 1
            value.removeAtIndex(lastUsed)
            iconFour.setImage(nil, forState: .Normal)
            updateToGo()
        }
    }
    
    @IBOutlet weak var iconFive: UIButton!
    @IBAction func icon5(sender: AnyObject) {
        if lastUsed == 5 {
            lastUsed = lastUsed - 1
            value.removeAtIndex(lastUsed)
            iconFive.setImage(nil, forState: .Normal)
            updateToGo()
        }
    }
    
    @IBOutlet weak var iconSix: UIButton!
    @IBAction func icon6(sender: AnyObject) {
        if lastUsed == 6 {
            lastUsed = lastUsed - 1
            value.removeAtIndex(lastUsed)
            iconSix.setImage(nil, forState: .Normal)
            updateToGo()
        }
    }
    
    @IBOutlet weak var iconSeven: UIButton!
    @IBAction func icon7(sender: AnyObject) {
        if lastUsed == 7 {
            lastUsed = lastUsed - 1
            value.removeAtIndex(lastUsed)
            iconSeven.setImage(nil, forState: .Normal)
            updateToGo()
        }
    }
    
    @IBOutlet weak var iconEight: UIButton!
    @IBAction func icon8(sender: AnyObject) {
        if lastUsed == 8 {
            lastUsed = lastUsed - 1
            value.removeAtIndex(lastUsed)
            iconEight.setImage(nil, forState: .Normal)
            updateToGo()
        }
    }
    
    @IBOutlet weak var iconNine: UIButton!
    @IBAction func icon9(sender: AnyObject) {
        if lastUsed == 9 {
            lastUsed = lastUsed - 1
            value.removeAtIndex(lastUsed)
            iconNine.setImage(nil, forState: .Normal)
            updateToGo()
        }
    }
    
    @IBOutlet weak var iconTen: UIButton!
    @IBAction func icon10(sender: AnyObject) {
        if lastUsed == 10 {
            lastUsed = lastUsed - 1
            value.removeAtIndex(lastUsed)
            iconTen.setImage(nil, forState: .Normal)
            updateToGo()
        }
    }
    
    @IBOutlet weak var iconEleven: UIButton!
    @IBAction func icon11(sender: AnyObject) {
        if lastUsed == 11 {
            lastUsed = lastUsed - 1
            value.removeAtIndex(lastUsed)
            iconEleven.setImage(nil, forState: .Normal)
            updateToGo()
        }
    }
    
    @IBOutlet weak var iconTwelve: UIButton!
    @IBAction func icon12(sender: AnyObject) {
        if lastUsed == 12 {
            lastUsed = lastUsed - 1
            value.removeAtIndex(lastUsed)
            iconTwelve.setImage(nil, forState: .Normal)
            updateToGo()
        }
    }
    
    @IBOutlet weak var waterToGoLbl: UILabel!
    @IBOutlet weak var goalLbl: UILabel!
    
    @IBAction func add8oz(sender: AnyObject) {
        if lastUsed < 12 {
            let image = UIImage(named: "water_glass90.png") as UIImage!
            buttons[lastUsed].setImage(image, forState: UIControlState.Normal)
            value.append(8)
            lastUsed = lastUsed + 1
            updateToGo()
        }
    }
    
    @IBAction func addBottle(sender: AnyObject) {
        if lastUsed < 12 {
            let image = UIImage(named: "water_bottle90.png") as UIImage!
            buttons[lastUsed].setImage(image, forState: UIControlState.Normal)
            value.append(16.8)
            lastUsed = lastUsed + 1
            updateToGo()
        }
    }
    
    @IBAction func getOtherValue(sender: AnyObject) {
        if lastUsed < 12 {
            let image = UIImage(named: "custom90.png") as UIImage!
            buttons[lastUsed].setImage(image, forState: UIControlState.Normal)
            value.append(custom)
            lastUsed = lastUsed + 1
            updateToGo()
        }
    }

    func userOther() {
        var alert = UIAlertController(title: "Custom Amount", message: "Enter oz drank", preferredStyle: .Alert)
        
        var input: String! = " "
        
        func completionHandler() {
            
            if !input.isEmpty {
                self.custom = (input as NSString).doubleValue
            }
            else {
                self.custom = 0.0
            }
        }
        
        alert.addTextFieldWithConfigurationHandler({ (textField) -> Void in textField.text = "0"})
        
        alert.addAction(UIAlertAction(title: "OK", style: .Default, handler: { (action) -> Void in
            let textField = alert.textFields![0] as! UITextField;
            input = textField.text;
        }))

        self.presentViewController(alert, animated: true, completion: completionHandler)
        
    }
    
    var lastUsed = 0
    var custom: Double! = 0.0
    
    @IBOutlet var buttons: [UIButton]!
    
    var value: [Double] = []
    
    func updateToGo() {
        var toGo = 0 as Double!
        for n in value {
            toGo = toGo + n
        }
        toGo = Double(128) - toGo
        toGo = Double(round(10*toGo)/10)
        
        if toGo > 0 {
            waterToGoLbl.text = "\(String(stringInterpolationSegment: toGo)) oz"
        }
        else {
            waterToGoLbl.text = "\(String(0)) oz"
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        // Core Data Entities
        let cdSettings = NSEntityDescription.insertNewObjectForEntityForName("Settings", inManagedObjectContext: self.managedObjectContext!) as! Settings
        let cdProfile = NSEntityDescription.insertNewObjectForEntityForName("Profile", inManagedObjectContext: self.managedObjectContext!) as! Profile
        let cdWater = NSEntityDescription.insertNewObjectForEntityForName("Water", inManagedObjectContext: self.managedObjectContext!) as! Water
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}