//
//  SettingsVC.swift
//  GOYFA
//
//  Created by Kevin Crowl on 5/3/15.
//  Copyright (c) 2015 Kevin Crowl. All rights reserved.
//

import UIKit
import CoreData

class SettingsVC: UIViewController {
    
    // Core Data Object
    let managedObjectContext = (UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext
    
    var cdProfile: Profile!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Core Data Entities
        let cdSettings = NSEntityDescription.insertNewObjectForEntityForName("Settings", inManagedObjectContext: self.managedObjectContext!) as! Settings
        let cdProfile = NSEntityDescription.insertNewObjectForEntityForName("Profile", inManagedObjectContext: self.managedObjectContext!) as! Profile
        
        //Fetch & Display
        let entityDescription = NSEntityDescription.entityForName("Profile", inManagedObjectContext: managedObjectContext!)
        
        let request = NSFetchRequest()
        request.entity = entityDescription
        
        
        if request.entity!.superentity?.name != nil {
        
        let pred = NSPredicate(format: "(name = %@)", nameInput.text)
        request.predicate = pred
        
        var error: NSError?
        
        var objects = managedObjectContext?.executeFetchRequest(request, error: &error)
        
        if let results = objects {
            
            if results.count > 0 {
                let match = results[0] as! NSManagedObject
                
                nameInput.text = match.valueForKey("name") as! String
                ageInput.text = match.valueForKey("age") as! String
                weightInput.text = match.valueForKey("weight") as! String
                heightInput.text = match.valueForKey("height") as! String
                strideInput.text = match.valueForKey("age") as! String
                
            }
            }
        }

    }
    
    @IBOutlet weak var nameInput: UITextField!
    @IBOutlet weak var ageInput: UITextField!
    @IBOutlet weak var weightInput: UITextField!
    @IBOutlet weak var heightInput: UITextField!
    @IBOutlet weak var strideInput: UITextField!
    
    @IBAction func saveButton(sender: AnyObject) {
        let entityDescription =
        NSEntityDescription.entityForName("Profile",
            inManagedObjectContext: managedObjectContext!)
        
        let cdProfile = Profile(entity: entityDescription!,
            insertIntoManagedObjectContext: managedObjectContext)
        
        cdProfile.name = nameInput.text
        cdProfile.age = (ageInput.text as NSString).doubleValue
        cdProfile.weight = (weightInput.text as NSString).doubleValue
        cdProfile.height = (heightInput.text as NSString).doubleValue
        cdProfile.stride = (strideInput.text as NSString).doubleValue
        
        var error: NSError?
        
        managedObjectContext?.save(&error)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
