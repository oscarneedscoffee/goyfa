//
//  FAMMonitor.swift
//  GOYFA
//
//  Created by Kevin Crowl on 4/15/15.
//  Copyright (c) 2015 Kevin Crowl. All rights reserved.
//

import UIKit
import CoreData
import CoreMotion

protocol FAMMonitorDelegate{
    func didSaveSittingTime(controller: FAMMonitor)
}

class FAMMonitor: UIViewController {
    
    lazy var motionManager = CMMotionManager()
    
    // Core Data Object
    let managedObjectContext = (UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext
    
    @IBOutlet weak var idleTimeLbl: UILabel!
    @IBOutlet weak var timeLeftLbl: UILabel!
    
    @IBOutlet weak var pauseButton: UIBarButtonItem!
    var maxSittingTime = 3
    var isPaused = true
    
    @IBAction func snooze(sender: AnyObject) {
        self.pause(self)
    }
    
    @IBAction func pause(sender: AnyObject) {
        if(self.isPaused == true){
            motionManager.stopAccelerometerUpdates()
            
            let alertController = UIAlertController(title: "FAMM",
                message: "Monitor Paused",
                preferredStyle: UIAlertControllerStyle.Alert)
            let okAction = UIAlertAction(title: "OK",
                style: UIAlertActionStyle.Cancel, handler: nil)
            
            alertController.addAction(okAction)
            presentViewController(alertController, animated: true,
                completion: nil)
            self.isPaused = false
            
            //user presses "Restart" to re-enable "Pause" button
            pauseButton.enabled = false
        }else{
            motionManager.startAccelerometerUpdates()
            restart(self)
            self.isPaused = true
        }
        
        
    }
    
    @IBAction func restart(sender: AnyObject) {
        motionManager.startAccelerometerUpdates()

        idleTimeLbl.text = ("\(String(self.maxSittingTime)) hours")
        timeLeftLbl.text = ("\(String(self.maxSittingTime)) hours 0 mins")
        
        let alertController = UIAlertController(title: "GOYFA",
            message: "Monitor Restarted",
            preferredStyle: UIAlertControllerStyle.Alert)
        let okAction = UIAlertAction(title: "OK",
            style: UIAlertActionStyle.Cancel, handler: nil)
        
        alertController.addAction(okAction)
        presentViewController(alertController, animated: true,
            completion: nil)
        
        //Re-enable pause button
        pauseButton.enabled = true
    }
    
    
    @IBOutlet var inputFields: [UITextField]!
    
    var delegate: FAMMonitorDelegate?
    
    private let fieldNames = ["maxSittingTime", "numReminders", "reminderInterval"]

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Core Data Entities
        let cdSettings = NSEntityDescription.insertNewObjectForEntityForName("Settings", inManagedObjectContext: self.managedObjectContext!) as! Settings
        let cdProfile = NSEntityDescription.insertNewObjectForEntityForName("Profile", inManagedObjectContext: self.managedObjectContext!) as! Profile
        let cdFAM = NSEntityDescription.insertNewObjectForEntityForName("FAM", inManagedObjectContext: self.managedObjectContext!) as! FAM
        
        if motionManager.accelerometerAvailable{
            let queue = NSOperationQueue()
            motionManager.startAccelerometerUpdatesToQueue(queue, withHandler:
                {(data: CMAccelerometerData!, error: NSError!) in
                    
                    println("X = \(data.acceleration.x)")
                    println("Y = \(data.acceleration.y)")
                    println("Z = \(data.acceleration.z)")
                    
                    self.idleTimeLbl.text = "not idle"
                }
            )
        } else {
            if var label = idleTimeLbl{
                label.text = "Idle for more than 2 hours"
            }
            println("Accelerometer is not available")
        }
    
        
    
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func saveButtonPressed(sender: AnyObject) {
        // ensure that UITextFields are not empty
        if inputFields[0].text.isEmpty{
            // create UIAlertController to display error message
            let alertController = UIAlertController(title: "Error",
                message: "Enter a number",
                preferredStyle: UIAlertControllerStyle.Alert)
            let okAction = UIAlertAction(title: "OK",
                style: UIAlertActionStyle.Cancel, handler: nil)
            alertController.addAction(okAction)
            presentViewController(alertController, animated: true,
                completion: nil)
            
        }
//        } else {
//            // update the Contact using NSManagedObject method setValue
//            for i in 0..<fieldNames.count {
//                let value = (!inputFields[i].text.isEmpty ?
//                    inputFields[i].text : nil)
//                //self.contact?.setValue(value, forKey: fieldNames[i])
//            }
            
            //self.delegate?.didSaveSittingTime(self)
        }
 
        
    
    
}

