//
//  FAM.swift
//  GOYFA
//
//  Created by Kevin Crowl on 5/3/15.
//  Copyright (c) 2015 Kevin Crowl. All rights reserved.
//

import Foundation
import CoreData

class FAM: NSManagedObject {

    @NSManaged var maxSit: NSNumber
    @NSManaged var notifications: NSNumber
    @NSManaged var numReminder: NSNumber
    @NSManaged var reminder: NSNumber

}
