//
//  Profile.swift
//  GOYFA
//
//  Created by Kevin Crowl on 5/3/15.
//  Copyright (c) 2015 Kevin Crowl. All rights reserved.
//

import Foundation
import CoreData

class Profile: NSManagedObject {

    @NSManaged var age: NSNumber
    @NSManaged var height: NSNumber
    @NSManaged var name: String
    @NSManaged var stride: NSNumber
    @NSManaged var weight: NSNumber

}
