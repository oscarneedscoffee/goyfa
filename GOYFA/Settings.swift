//
//  Settings.swift
//  GOYFA
//
//  Created by Kevin Crowl on 5/3/15.
//  Copyright (c) 2015 Kevin Crowl. All rights reserved.
//

import Foundation
import UIKit
import CoreData

class Settings: NSManagedObject {

    @NSManaged var global: NSNumber
    @NSManaged var newDay: NSDecimalNumber
    @NSManaged var sound: NSNumber
    @NSManaged var vibrate: NSNumber

}
