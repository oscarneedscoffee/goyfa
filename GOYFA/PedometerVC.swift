//
//  PedometerVC.swift
//  GOYFA
//
//  Created by Kevin Crowl on 4/15/15.
//  Copyright (c) 2015 Kevin Crowl. All rights reserved.
//

import UIKit
import CoreData
import CoreMotion

class PedometerVC: UIViewController {
 
    
    // Core Data Object
    let managedObjectContext = (UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext
    
    @IBOutlet weak var distanceLbl: UILabel!
    @IBOutlet weak var caloriesLbl: UILabel!
    @IBOutlet weak var stepsLbl: UILabel!
    @IBOutlet weak var tripStepsLbl: UILabel!
    @IBOutlet weak var tripDistanceLbl: UILabel!
    @IBOutlet weak var tripDurationLbl: UILabel!
    @IBOutlet weak var tripSpeedLbl: UILabel!
    
    @IBOutlet weak var pauseButton: UIBarButtonItem!
    var isPaused = true
    
    
//    var stepsTaken:[Int] = []
    let dataProcessingQueue = NSOperationQueue()
    var myPedometer      = CMPedometer()
    var activityManager  = CMMotionActivityManager()
    
    @IBAction func enterTime(sender: AnyObject) {
    }
    
    @IBAction func enterDistance(sender: AnyObject) {
    }
    
    @IBAction func pause(sender: AnyObject) {
        
        let appDelegate =
        UIApplication.sharedApplication().delegate as! AppDelegate
        
        if(self.isPaused == true){
        
       myPedometer.stopPedometerUpdates()
        let alertController = UIAlertController(title: "GOYFA",
            message: "Pedometer Paused",
            preferredStyle: UIAlertControllerStyle.Alert)
        let okAction = UIAlertAction(title: "OK",
            style: UIAlertActionStyle.Cancel, handler: nil)
        alertController.addAction(okAction)
        presentViewController(alertController, animated: true,
            completion: nil)
            
            self.isPaused = false
            pauseButton.title = "Go!"
        }else{
           // appDelegate.applicationDidBecomeActive(appDelegate.pedometer)
            self.isPaused = true
            pauseButton.title = "Pause"
        }
        
        //activityManager.stopActivityUpdates()
        
    }
    
    @IBAction func reset(sender: AnyObject) {
        if CMPedometer.isStepCountingAvailable(){
            myPedometer.startPedometerUpdatesFromDate(NSDate(), withHandler: { (data: CMPedometerData!, error: NSError!) in
            println("Number of steps = \(data.numberOfSteps)")
            })
        }else{
            println("Step counting is not available pedometervc reset()")
        }
        
        let alertController = UIAlertController(title: "GOYFA",
            message: "Pedometer Reset",
            preferredStyle: UIAlertControllerStyle.Alert)
        let okAction = UIAlertAction(title: "OK",
            style: UIAlertActionStyle.Cancel, handler: nil)
        alertController.addAction(okAction)
        presentViewController(alertController, animated: true,
            completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Core Data Entities
        let cdSettings = NSEntityDescription.insertNewObjectForEntityForName("Settings", inManagedObjectContext: self.managedObjectContext!) as! Settings
        let cdProfile = NSEntityDescription.insertNewObjectForEntityForName("Profile", inManagedObjectContext: self.managedObjectContext!) as! Profile
        
        startActivityManager()
        getSteps()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func getSteps(){
        if CMPedometer.isStepCountingAvailable(){
            
            myPedometer.queryPedometerDataFromDate(NSDate.tenMinutesAgo(),
                toDate: NSDate.now(),
                withHandler: {(data: CMPedometerData!, error: NSError!) in
                    
                    self.tripStepsLbl.text = ("\(data.numberOfSteps) steps")
                    
            })
            
        } else {
            //when going to another view, label may return nil. 
            //the if statement prevents a runtime error
            if var label = tripStepsLbl{
                label.text = "0 steps"
                println("pedometer unavailable in PedometerVC.getSteps()")
            }
        }
        

        
    }
    
    func startActivityManager(){
        self.activityManager.startActivityUpdatesToQueue(dataProcessingQueue) {
            data in
            dispatch_async(dispatch_get_main_queue()) {
                if data.running {
                    self.tripStepsLbl.text = "0 steps"
                }
            }
        }
    }
    
    
}