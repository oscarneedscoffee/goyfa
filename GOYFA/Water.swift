//
//  Water.swift
//  GOYFA
//
//  Created by Kevin Crowl on 5/3/15.
//  Copyright (c) 2015 Kevin Crowl. All rights reserved.
//

import Foundation
import CoreData

class Water: NSManagedObject {

    @NSManaged var goal: NSNumber
    @NSManaged var notification: NSNumber
    @NSManaged var numReminder: NSNumber
    @NSManaged var reminder: NSNumber

}
