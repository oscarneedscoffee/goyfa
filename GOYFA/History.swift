//
//  History.swift
//  GOYFA
//
//  Created by Kevin Crowl on 5/3/15.
//  Copyright (c) 2015 Kevin Crowl. All rights reserved.
//

import Foundation
import CoreData

class History: NSManagedObject {

    @NSManaged var avgInactive: NSNumber
    @NSManaged var date: NSDate
    @NSManaged var drank: NSNumber
    @NSManaged var steps: NSNumber

    class func createInManagedObjectContext(moc: NSManagedObjectContext, avgInactive: NSNumber, date: NSDate, drank: NSNumber, steps: NSNumber ) -> History {
        let cdHistory = NSEntityDescription.insertNewObjectForEntityForName("History", inManagedObjectContext: moc) as! History
        cdHistory.date = date
        cdHistory.steps = steps
        cdHistory.drank = drank
        cdHistory.avgInactive = avgInactive
        return cdHistory
    }
}
