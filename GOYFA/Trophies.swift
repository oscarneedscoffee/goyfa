//
//  Trophies.swift
//  GOYFA
//
//  Created by Kevin Crowl on 5/3/15.
//  Copyright (c) 2015 Kevin Crowl. All rights reserved.
//

import Foundation
import UIKit
import CoreData

class Trophies: NSManagedObject {

    @NSManaged var trophy: NSNumber

}
