//
//  TrophyVC.swift
//  GOYFA
//
//  Created by Kevin Crowl on 5/4/15.
//  Copyright (c) 2015 Kevin Crowl. All rights reserved.
//

import UIKit
import CoreData

class TrophyVC: UITableViewController {
    
    // Core Data Object
    let managedObjectContext = (UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Core Data Entities
        let cdTrophy = NSEntityDescription.insertNewObjectForEntityForName("Trophies", inManagedObjectContext: self.managedObjectContext!) as! Trophies
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}